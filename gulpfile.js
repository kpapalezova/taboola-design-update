// npm install gulp gulp-less gulp-watch gulp-autoprefixer gulp-plumber gulp-livereload gulp-less path --save-dev
// explanation task breakdown: http://stackoverflow.com/questions/23953779/gulp-watch-and-compile-less-files-with-import
var gulp = require('gulp');
var less = require('gulp-less');
var watch = require('gulp-watch');
var prefix = require('gulp-autoprefixer');
var plumber = require('gulp-plumber');
var minifycss = require('gulp-minify-css');
var path = require('path');
var browserSync = require('browser-sync').create();
var htmlInjector = require('bs-html-injector');
var reload = browserSync.reload;

// Use With Browser Sync
gulp.task('browser-sync', function() {

  browserSync.use(htmlInjector, {});

  gulp.src('./css/style.less')  // only compile the entry file
    .pipe(plumber())
    .pipe(less({
      paths: ['./css/']
    }))
    .pipe(prefix("last 8 version", "> 1%", "ie 9", "ie 8", "ie 7", "ios 6"))
    .pipe(plumber.stop())
    .pipe(minifycss({keepBreaks: false, advanced: false, agressiveMerging: false, compatibility: 'ie7'}))
    .pipe(gulp.dest('./css'))
    .pipe(reload({stream: true}));
});

gulp.task('less', function() {
  gulp.src('./css/style.less')  // only compile the entry file
    .pipe(plumber())
    .pipe(less({
      paths: ['./css/']
    } ))
    .pipe(prefix("last 8 version", "> 1%", "ie 9", "ie 8"))
    .pipe(plumber.stop())
    .pipe(minifycss({keepBreaks: false, advanced: false, agressiveMerging: false, compatibility: 'ie8'}))
    .pipe(gulp.dest('./css'));
});

gulp.task('watch', function() {

  gulp.watch(['./css/*.less'], ['less']);
  gulp.watch(['**/*.js', '!node_modules/**/*.js']);
  gulp.watch(['template.php','templates/*.php']);

});

// In the command line type 'gulp serve' for browsersync
gulp.task('serve', ['browser-sync'], function() {

  browserSync.init({
    proxy: 'http://leadgen.dev'
  });

  gulp.watch(['./css/*.less'], ['browser-sync']);
  gulp.watch(['**/*.js', '!node_modules/**/*.js']).on('change', reload);
  gulp.watch(['template.php','templates/*.php'], htmlInjector);
});

gulp.task('default', ['watch']); // Default will run the 'entry' watch task
