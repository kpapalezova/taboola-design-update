(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([], factory(root));
  } else if (typeof exports === 'object') {
    module.exports = factory(root);
  } else {
    root.svgMapAttacher = factory(root);
  }
})(typeof global !== "undefined" ? global : this.window || this.global, function(root) {
  'use strict';

  function supportsSvg() {
    return document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#BasicStructure", "1.1");
  }

  function setRegionMapAsSvg(el) {
    var ajax = new XMLHttpRequest();
    ajax.open("GET", el.dataset.countrySvg, true);
    ajax.send();

    ajax.onreadystatechange = function() {
      if (ajax.readyState === XMLHttpRequest.DONE && ajax.status === 200) {
        var svgReplacement = document.createElement("div");
        svgReplacement.className += "region-map";
        svgReplacement.innerHTML = ajax.responseText;
        el.parentNode.replaceChild(svgReplacement, el);
        var mapRegions = svgReplacement.querySelectorAll('.map-region');
        Array.prototype.map.call(mapRegions, setToolTipForRegion);

        setRegionAnchors(el, svgReplacement);
      }
    };
  }

  function setRegionAnchors(oldEl, newEl) {
    var regionUrl = oldEl.dataset.regionsLink;
    var svgAnchors = newEl.querySelectorAll('svg a');
    Array.prototype.forEach.call(svgAnchors, function(value) {
      value.setAttribute("xlink:href", regionUrl);
    });
  }

  function setToolTipForRegion(region) {
    region.addEventListener('mouseover', function() {
      var tip = this.getAttribute('title');
      swapTitleAttr(this);
      addToolTip(this, tip);
    }, false);

    region.addEventListener('mouseout', function() {
      swapTitleAttr(this);
      var toolTip = document.querySelector('#region-map-tooltip');
      toolTip.parentNode.removeChild(toolTip);
    }, false);

    region.addEventListener('mousemove', function(e) {
      var toolTip = document.querySelector('#region-map-tooltip');
      setPosition(toolTip, e.pageX, e.pageY);
    }, false);
  }

  function addToolTip(el, tip) {
    var toolTip = document.createElement('span');
    toolTip.id = 'region-map-tooltip';
    toolTip.innerHTML = tip;

    document.body.appendChild(toolTip);
    fadeIn(toolTip);
  }

  function swapTitleAttr(el) {
    if (el.hasAttribute('data-title')) {
      el.setAttribute('title', el.getAttribute('data-title'));
    } else {
      var title = el.getAttribute('title');
      el.setAttribute('data-title', title);
      el.removeAttribute('title');
    }
  }

  function setPosition(el, x, y) {
    el.style.top = y + 10 + "px";
    el.style.left = x + 20 + "px";
  }

  function fadeIn(el) {
    el.style.opacity = 0;
    el.style.display = 'block';

    var last = +new Date();

    var tick = function() {
      el.style.opacity = +el.style.opacity + (new Date() - last) / 400;
      last = +new Date();

      if (+el.style.opacity < 1) {
        (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
      } else {
        el.style.opacity = '';
      }
    };

    tick();
  }

  function init() {
    if (supportsSvg()) {

      var svgs = document.querySelectorAll('[data-country-svg]');
      if (svgs.length) {
        Array.prototype.map.call(svgs, setRegionMapAsSvg);
      }
    }
  }

  document.addEventListener("DOMContentLoaded", init, false);

  return;

});
