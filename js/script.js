//doc ready
$(function() {
  "use strict";

  var keycodeEsc = 27,
      keyEnter = 13,
      keySpace = 32,
      windowWidthSwitch = 991;

  /* show and hide full menu on smaller screens */
  function toggleMobileMenu() {
    var menuNav = $('#main-menu'),
        menuBtn = $('#hamburger-btn');

    if (menuNav.hasClass('hidden-menu-mob')) {

      menuNav.removeClass('nondisplay-menu-mob');
      menuBtn.attr('aria-expanded', 'true');
      menuBtn.attr('aria-pressed', 'true');

      /* Need to allow the DOM to render the removal of non-disaply class before removing class that results in animation */
      setTimeout(function() {
        menuNav.removeClass('hidden-menu-mob');
      }, 0);
    }
    else {
      menuNav.addClass('hidden-menu-mob');
      menuBtn.attr('aria-expanded', 'false');

      setTimeout(function() {
        menuBar.addClass('nondisplay-menu-mob');
      }, 300);
    }
  }

  $(function toggleMenu() {

    $('.menu-togglebtn').click(function() {
      toggleMobileMenu();
    });

    /* on 'esc', close mobile menu */
    $('body').keydown(function(e) {
      if (e.keyCode == keycodeEsc && $(window).width() <= windowWidthSwitch && !$('#main-menu').hasClass('hidden-menu-mob')) {
        toggleMobileMenu();
        $('.menu-togglebtn').focus();
      }
    });

    /* allow screenreader users to skip navigation */
    $('#skipNav').click(function() {
      $('#skipNavTarget').focus();
    });
  });

   stickyElement('.banner', -60);
   stickyElement('.share-icons', -60);
   //getLeadCount(0.5);
   updateSubmissionsCount('.submissions-count', 0.5, 'http://join.surveycompare.net/api/data');

   var url = window.location.search.split(1);
   var $head = $('head');
   var $body = $('body');

   if ($body.hasClass('surveycompare')) {
     $head.find('link[rel="shortcut icon"]').attr('href', '/sites/all/themes/taboola_x_v02/images/sc-favicon.ico');
   }
   else if ($body.hasClass('expertsinmoney')) {
     $head.find('link[rel="shortcut icon"]').attr('href', '/sites/all/themes/taboola_x_v02/images/eim-favicon.ico');
   }
   else if ($body.hasClass('expertmarket')) {
     $head.find('link[rel="shortcut icon"]').attr('href', '/sites/all/themes/taboola_x_v02/images/em-favicon.ico');
   }
   else if ($body.hasClass('cliniccompare')) {
     $head.find('link[rel="shortcut icon"]').attr('href', '/sites/all/themes/taboola_x_v02/images/cc-favicon.ico');
   }

   if (url[0] == '?sid=') {
     $('#exit-modal-container').show();
   }

   if ($('body').hasClass('modal-webform-enabled')) {
     $('.webform-client-form').appendTo('#exit-modal').show();
     $('.privacy').insertAfter('#question-controls');
   }
});

function stickyElement(elClass, offsetY) {
  var stickyEl = $(elClass),
      stickyPoint = stickyEl.offset().top + offsetY;

  $(window).scroll(function() {
    if ($(window).scrollTop() > stickyPoint) {
      stickyEl.addClass('sticky');
      $('.before-webform').addClass('webform-margin');
    }
    else {
      stickyEl.removeClass('sticky');
      $('.before-webform').removeClass('webform-margin');
    }
  });
}

// dynamic api submissions counter
function updateSubmissionsCount(targetEl, mins, url) {
  setSubmissionsCount(targetEl, url);
  setInterval(function() {
    setSubmissionsCount(targetEl, url);
  }, 1000*60*mins);
}

function setSubmissionsCount(targetEl, url) {
  $.get(url, function(data) {
    if (data != null) {
      var json = JSON.parse(data);
      $(targetEl).text(json.submissions_count);
    }
  });
}
//end counter funcs
