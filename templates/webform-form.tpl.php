<?php

/**
 * @file
 * Customize the display of a complete webform.
 *
 * This file may be renamed "webform-form-[nid].tpl.php" to target a specific
 * webform on your site. Or you can leave it "webform-form.tpl.php" to affect
 * all webforms on your site.
 *
 * Available variables:
 * - $form: The complete form array.
 * - $nid: The node ID of the Webform.
 *
 * The $form array contains two main pieces:
 * - $form['submitted']: The main content of the user-created form.
 * - $form['details']: Internal information stored by Webform.
 */
?>

<?php
	if(isset($_GET['sid']) && $_GET['sid'] > 0) {
		// Display the confirmation
		print '<div id="webform-referer-confirmation">';
			$message = preg_replace('/^referer:/', '', $form['#node']->webform['confirmation']);
		  $message = preg_replace('/!reference/', $_GET['sid'], $message);
			$message = check_markup($message, $form['#node']->webform['confirmation_format'], FALSE);
			print $message;
		print '</div>';
	} else {
		// Display the form
		// If editing or viewing submissions, display the navigation at the top.
		if (isset($form['submission_info']) || isset($form['navigation'])) {
		  print drupal_render($form['navigation']);
		  print drupal_render($form['submission_info']);
		}

		// Print out the custom header here
		if(strlen($form['#node']->webform_mods['heading']) > 0) {
			print '<hgroup><h2>'.$form['#node']->webform_mods['heading'].'</h2></hgroup>';
		}

		// Print out the main part of the form.
		// Feel free to break this up and move the pieces within the array.
		print drupal_render($form['submitted']);

		// Always print out the entire $form. This renders the remaining pieces of the
		// form that haven't yet been rendered above.
		print drupal_render($form);

		// Print out the navigation again at the bottom.
		if (isset($form['submission_info']) || isset($form['navigation'])) {
		  unset($form['navigation']['#printed']);
		  print drupal_render($form['navigation']);
		}
	}
