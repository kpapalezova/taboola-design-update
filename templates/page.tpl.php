<?php
  $path = base_path() . path_to_theme();
?>
<!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>"> <![endif]-->
<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>"> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <title><?php print $head_title; ?></title>
    <?php print $head . $styles . $scripts; ?>
    <?php if ($language->language == 'ar'): ?>
      <link href="<?php print $path; ?>/css/style-rtl.css" type="text/css" rel="stylesheet/css" />
    <?php endif; ?>
  </head>

  <?php global $webform_page_number; ?>
  <body class="<?php print $body_classes; ?> content-<?php print $language->language ?> webform-page-number-<?php print $webform_page_number ?>">
    <?php print $content; ?>
    <?php print $closure; ?>
  </body>
</html>
