<?php
  // Print all Custom CSS & Scripts
  if (!empty($node->field_css[0]['view'])):
    print $node->field_css[0]['view'];
  endif;
?>

<header id="section-header" role="banner">
  <div class="block-group page-width">

    <div class="block" id="advert">
      <h3>
        <?php print t('Advertorial'); ?>
      </h3>
    </div>

    <div class="block-group" id="section-menu">
      <?php print $node->field_header[0]['view']; ?>
    </div>
  </div>
</header>

<main id="main-content" role="main">

  <section id="section-banner">
    <div class="block-group">
      <div class="block-group webform-footer">
        <?php print $node->field_webform_footer[0]['view']; ?>
      </div>
      <header class="block-group webform-header">
        <div class="page-width">
          <?php print $node->field_webform_header[0]['view']; ?>
        </div>
      </header>
    </div>
  </section>

  <section id="section-content">
    <div class="block-group page-width">

      <div class="block share-icons">
        <!-- Go to www.addthis.com/dashboard to customize (Karina Papalezova MVF google login) -->
        <div class="addthis_inline_share_toolbox"></div>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58760a2823879e31"></script>
      </div>

      <article class="block before-webform">
        <?php print $node->field_before_webform[0]['view']; ?>
      </article>

      <aside class="block after-webform">
        <div class="banner">
          <?php print $node->field_after_webform[0]['view']; ?>
        </div>
      </aside>

    </div>
  </section>

  <section id="section-lower-body">
    <div class="block-group page-width">
      <div class="block secondary-cta">
        <?php print $node->field_lower_body[0]['view']; ?>
      </div>
    </div>
  </section>

</main>

<footer id="section-footer">
  <div class="block-group">
    <?php print $node->field_footer[0]['view']; ?>
  </div>
</footer>

<section id="section-disclaimer">
  <div class="block-group page-width">
    <div class="disclaimer block">
      <?php print t('This is a sponsored article and is intended as an advertisement, not editorial content.'); ?> <!-- Disclaimer -->
    </div>
  </div>
</section>
